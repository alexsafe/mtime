package com.foanapps.alexandru.mtime;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.databind.JsonNode;
import com.foanapps.alexandru.mtime.ServicesReceivers.MTrackingService;
import com.foanapps.alexandru.mtime.fragments.CurrentFragment;
import com.foanapps.alexandru.mtime.fragments.HistoryFragment;
import com.foanapps.alexandru.mtime.fragments.HomeFragment;
import com.foanapps.alexandru.mtime.fragments.SettingsFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import allbegray.slack.SlackClientFactory;
import allbegray.slack.rtm.Event;
import allbegray.slack.rtm.EventListener;
import allbegray.slack.rtm.SlackRealTimeMessagingClient;
import allbegray.slack.type.Authentication;
import allbegray.slack.webapi.SlackWebApiClient;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.services.concurrency.AsyncTask;

import static com.foanapps.alexandru.mtime.utils.Common.MY_PERMISSIONS_ACCESS_FINE_LOCATION;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    private static String TAG = "MainActiv";
    BottomBar bottomBar;
    private TextView messageView;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

//        new DoSlackStuffASync().execute();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // ask for location permissions for marshmellow and above versions
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs location permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_ACCESS_FINE_LOCATION);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_ACCESS_FINE_LOCATION);
                }

            }

        } else {
            startService(new Intent(this, MTrackingService.class));
        }

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        bottomBar.setDefaultTabPosition(0);

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int position) {
                switch (position) {
                    case R.id.tab_map:
                        commitFragment(new HomeFragment());
                        break;
                    case R.id.tab_settings:
                        commitFragment(new SettingsFragment());
                        break;
                    case R.id.tab_current:
                        commitFragment(new CurrentFragment());
                        break;
                    case R.id.tab_history:
                        commitFragment(new HistoryFragment());
                        break;
                }
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(int tabId) {
                Toast.makeText(getApplicationContext(), " " + tabId, Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("Test", "request code:" + requestCode);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == MY_PERMISSIONS_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startService(new Intent(this, MTrackingService.class));
            } else {
                // permission denied
                Toast.makeText(MainActivity.this, "Permission denied to read your position", Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Test", "request code onActivityResult:" + requestCode);
        if (requestCode == MY_PERMISSIONS_ACCESS_FINE_LOCATION) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                Log.d("Test", "request code permission:" + requestCode);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        bottomBar.onSaveInstanceState();
    }

    private void commitFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    class DoSlackStuffASync extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {

            final String slackToken = "xoxp-330116225777-330116226273-331820126228-2d8f5494cc1b54c3ae405f47a916242b";

            SlackWebApiClient mWebApiClient = SlackClientFactory.createWebApiClient(slackToken);
            JsonNode messagingApiNode= mWebApiClient.startRealTimeMessagingApi();
//            String webSocketUrl  = messagingApiNode.findPath("url").asText();
            String webSocketUrl = "wss:\\/\\/cerberus-xxxx.lb.slack-msgs.com\\/websocket\\/t3ZTPLSjdRuHir5pdEChCJErA5dH-LpdAche5zOMWncxB9pgJrOgMQG3xDOBerwHn-SwFgjXxb6ACsgfzKoi-V3aCpoXYKEq0Puc3AV6e3fbkwfSxbE33zbDfGOm0WRVo2c9QqAW\\/2?dp=1";
            SlackRealTimeMessagingClient mRtmClient = new SlackRealTimeMessagingClient(webSocketUrl);

            mRtmClient.addListener(Event.HELLO, new EventListener() {

                @Override
                public void onMessage(JsonNode message) {
                    Authentication authentication = mWebApiClient.auth();
                    String mBotId = authentication.getUser_id();
                    Log.d(TAG, "onMessage :" + mBotId);
                    Log.d(TAG, "User id: " + mBotId);
                    Log.d(TAG, "Team name: " + authentication.getTeam());
                    Log.d(TAG, "User name: " + authentication.getUser());
                }
            });


            mRtmClient.addListener(Event.MESSAGE, new EventListener() {

                @Override
                public void onMessage(JsonNode message) {
                    Log.d(TAG, "message listener:" + message);
                }
            });

            mRtmClient.connect();
            return "muie";
        }
    }
}
