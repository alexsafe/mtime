package com.foanapps.alexandru.mtime;

import android.app.Application;
import android.content.res.Configuration;
import android.os.Bundle;

import com.foanapps.alexandru.mtime.models.Place;
import com.orm.SugarApp;
import com.orm.SugarContext;

/**
 * Created by alexandru on 3/13/2017.
 */

public class MTimeApplication  extends SugarApp {


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());
        Place.findById(Place.class, (long) 1);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
