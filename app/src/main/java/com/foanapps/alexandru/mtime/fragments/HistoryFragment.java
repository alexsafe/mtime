package com.foanapps.alexandru.mtime.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment;
import com.foanapps.alexandru.mtime.R;
import com.foanapps.alexandru.mtime.models.Place;
import com.foanapps.alexandru.mtime.presenters.MTrackPresenter;

import java.util.List;

import static com.foanapps.alexandru.mtime.utils.Common.HQ;

/**
 * Created by alexandru on 3/11/2017.
 */

public class HistoryFragment extends Fragment implements DatePickerDialogFragment.DatePickerDialogHandler {


    TextView log;
    Place hq;
    List<Place> logList;
    MTrackPresenter presenter;

    ImageButton startDate;
    ImageButton endDate;

    String filterStartDate;
    String filterEndDate;

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        logList = Place.listAll(Place.class);
        presenter = new MTrackPresenter(getContext());
        filterStartDate = "";
        filterEndDate = "";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        log = (TextView) view.findViewById(R.id.log);
//        startDate = (ImageButton) view.findViewById(R.id.startDate);
//        endDate = (ImageButton) view.findViewById(R.id.endDate);

//        startDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DatePickerBuilder dpb = new DatePickerBuilder()
//                        .setReference(1)
//                        .setFragmentManager(getChildFragmentManager())
//                        .setStyleResId(R.style.BetterPickersDialogFragment)
//                        .setTargetFragment(HistoryFragment.this)
//                        .setYearOptional(true);
//                dpb.show();
//            }
//        });

//        endDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DatePickerBuilder dpb = new DatePickerBuilder()
//                        .setReference(2)
//                        .setFragmentManager(getChildFragmentManager())
//                        .setStyleResId(R.style.BetterPickersDialogFragment)
//                        .setTargetFragment(HistoryFragment.this)
//                        .setYearOptional(true);
//                dpb.show();
//            }
//        });

        log.setText("In development");
//        if (log!=null) {
//            log.setText(getString(R.string.history_size,logList.size() ));
//        }

//        for (Place event : logList) {
//            Log.d("test", "placedata ------------");
//            Log.d("test", "placedata event.getName():" + event.getName());
//            Log.d("test", "placedata event.getDay():" + event.getDay());
//            Log.d("test", "placedata event.getDuration():" + event.getDuration());
//            Log.d("test", "placedata event.getArrivedAt():" + event.getArrivedAt());
//            Log.d("test", "placedata =============");


//            log.append(event.getName()+" ");
//            log.append(event.getDay()+" ");
//            log.append(event.getDuration() + " ");
//            log.append(event.getArrivedAt() + " ");
//        }
    }

    @Override
    public void onDialogDateSet(int reference, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear+=1;
        Log.d("test", "selected date:" + reference + " " + year + " " + monthOfYear + " " + dayOfMonth);
        if (reference == 1){

            filterStartDate = monthOfYear+"/"+dayOfMonth+"/"+year;
            DatePickerBuilder dpb = new DatePickerBuilder()
                    .setReference(2)
                    .setFragmentManager(getChildFragmentManager())
                    .setStyleResId(R.style.BetterPickersDialogFragment)
                    .setTargetFragment(HistoryFragment.this)
                    .setYearOptional(true);
            dpb.show();
        }
        if (reference == 2){
            filterEndDate = monthOfYear+"/"+dayOfMonth+"/"+year;

            for (Place event : logList) {
                String day = event.getDay();

                Log.d("test","filter start:"+presenter.fromStringToDate(filterStartDate));
                Log.d("test","filter end:"+presenter.fromStringToDate(filterEndDate));
                Log.d("test","filter compare:"+presenter.fromStringToDate(day));

                if (presenter.fromStringToDate(filterStartDate).compareTo(presenter.fromStringToDate(day))<=0
                    &&
                    presenter.fromStringToDate(day).compareTo(presenter.fromStringToDate(filterEndDate))<=0
                    &&
                    event.getName().equals(HQ)
                ) {


                    Log.d("test", "placedatafilter ------------");
                    Log.d("test", "placedatafilter event.getName():" + event.getName());
                    Log.d("test", "placedatafilter event.getDay():" + event.getDay());
                    Log.d("test", "placedatafilter event.getDuration():" + event.getDuration());
                    Log.d("test", "placedatafilter event.getArrivedAt():" + event.getArrivedAt());
                    Log.d("test", "placedatafilter =============");
                }

//            log.append(event.getName()+" ");
//            log.append(event.getDay()+" ");
//            log.append(event.getDuration() + " ");
//            log.append(event.getArrivedAt() + " ");
            }
        }
    }
}
