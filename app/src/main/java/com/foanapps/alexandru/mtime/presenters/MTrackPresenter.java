package com.foanapps.alexandru.mtime.presenters;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.foanapps.alexandru.mtime.MainActivity;
import com.foanapps.alexandru.mtime.models.Place;
import com.foanapps.alexandru.mtime.utils.PersistentManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.foanapps.alexandru.mtime.utils.Common.HQ;

/**
 * Created by alexandru on 3/11/2017.
 */

public class MTrackPresenter {

    private long currentMilliTime;
    private Context mContext;
    private int durataPrecedenta;
    private long firstStart;
    private long start;

    public MTrackPresenter(Context context) {
        currentMilliTime = System.currentTimeMillis() / 1000;
        mContext = context;
    }

    public void appendLog(String text, String file) {
        File logFile = new File("sdcard/" + file);
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isSameDay(long dateToCompare) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        Date current = new Date(currentMilliTime * 1000L);
        Date compare = new Date(dateToCompare * 1000L);
        String currentDate = fmt.format(current);
        String dateToCompareString = fmt.format(compare);

        return currentDate.equals(dateToCompareString);
    }

    public boolean areSameDay(long date1, long date2) {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        Date current = new Date(date1 * 1000L);
        Date compare = new Date(date2 * 1000L);
        String currentDate = fmt.format(current);
        String dateToCompareString = fmt.format(compare);

        return currentDate.equals(dateToCompareString);
    }

    public Date fromStringToDate(String date){
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        try {
            return  formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public String getDateTime(long time) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            Date netDate = (new Date(time * 1000L));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "get Date exception:" + ex;
        }

    }


    public String getDate(long timeStamp) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp * 1000L));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "get Date exception:" + ex;
        }
    }

    public String transformLongToDateString(long timeStamp) {

        try {
            Date d = new Date(TimeUnit.SECONDS.toMillis(timeStamp));
            Log.d("test", "transformLongToDateString d:" + d);
            Log.d("test", "transformLongToDateString timeStamp:" + timeStamp);
            return d.toString();
        } catch (Exception ex) {
            Log.d("test", "transformLongToDateStringException:" + ex);
            return "null";
        }
    }

    public int calculateDiff(long start, long stop) {
        return (int) Math.abs(stop - start) / 60;
    }

    public String setLocation(String locationName, String prevLocationName) {
        String status = "";
        PersistentManager pm = new PersistentManager(mContext);
        currentMilliTime = System.currentTimeMillis() / 1000;
        String hours = String.valueOf(pm.getHomeDayTotal() / 60);
        String minutes = String.valueOf(pm.getHomeDayTotal() % 60);

        if (locationName.equals("void")) {

            if (prevLocationName.equals(HQ)) {
                status = "Left " + HQ + " ";
                pm.setStop(currentMilliTime);

                pm.setHomeDayTotal(calculateDiff(pm.getStart(), pm.getStop()) + pm.getPreviousDuration());
                pm.setPreviousDuration(pm.getHomeDayTotal());

            }
        } else {
            if (locationName.equals(HQ)) {

                if (prevLocationName.equals("void")) {
                    status = "Arrived at " + HQ + " ";
                    pm.setStart(currentMilliTime);

                    if (pm.isFirstStartOfTheDay()) {
                        pm.setRang(false);
                        putInDbAndInit(pm, "if");

                    }
                } else if (prevLocationName.equals(HQ)) {
                    if (!areSameDay(currentMilliTime, pm.getFirstStart())) {
                        putInDb(pm);
                        pm.setStart(currentMilliTime);
                        pm.setFirstStartOfTheDay(true);
                        pm.setFirstStart(currentMilliTime);
                        pm.setPreviousDuration(0);
                    } else {
                        pm.setFirstStartOfTheDay(false);
                    }
                }
            }
        }

        return status;
    }

    private void putInDbAndInit(PersistentManager pm, String type) {
        putInDb(pm);
        init(pm, type);
    }

    private void init(PersistentManager pm, String type) {
        pm.setFirstStartOfTheDay(false);
        pm.setFirstStart(currentMilliTime);
        pm.setPreviousDuration(0);
        pm.setRang(false);

        appendLog(type + " first start " + " at " + pm.getFirstStart(), "mtimestartlog.txt");
        appendLog(type + " history " + " arrived at: " + getDateTime(pm.getFirstStart()) +
                        " setDay: " + getDate(pm.getFirstStart()) +
                        " duration:" + pm.getHomeDayTotal()
                , "mtimehistorylog.txt");
    }

    private void putInDb(PersistentManager pm) {
        Place hq = new Place();
        hq.setArrivedAt(getDateTime(pm.getFirstStart()));
        hq.setName(HQ);
        hq.setDay(getDate(pm.getFirstStart()));
        hq.setDuration(pm.getPreviousDuration());
        hq.save();
    }

    public void sendNotification(String message, String location, Context context) {
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        //set
        builder.setContentIntent(contentIntent);
        builder.setSmallIcon(android.R.drawable.ic_dialog_info);
        builder.setContentText("Baaaa!!!");
        builder.setContentTitle(message + " la " + location);
        builder.setAutoCancel(true);
        builder.setDefaults(Notification.DEFAULT_ALL);

        Notification notification = builder.build();
        nm.notify((int) System.currentTimeMillis(), notification);
    }

    public int getHoursFromInterval(int interval) {
        return interval / 60;
    }

    public int getMinutesFromInterval(int interval) {
        return interval % 60;
    }
}
