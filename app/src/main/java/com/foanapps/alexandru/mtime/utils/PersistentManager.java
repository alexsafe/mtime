package com.foanapps.alexandru.mtime.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by alexandru on 3/11/2017.
 */

public class PersistentManager {
    //    private Context mContext;
    private SharedPreferences preferences;

    public PersistentManager(Context context) {
//        mContext=context;
        preferences = context.getSharedPreferences(context.getPackageName() + "_preferences", MODE_PRIVATE);
    }

    public String getLastLocation() {
        return preferences.getString("last_location", "void");
    }

    public void setLastLocation(String locationName) {
        preferences.edit().putString("last_location", locationName).apply();
    }

    public String getLastStatus() {
        return preferences.getString("last_status", "void");
    }

    public void setLastStatus(String status) {
        preferences.edit().putString("last_status", status).apply();
    }

    public long getStart() {
        return preferences.getLong("start", 0L);
    }

    public void setStart(long startTimer) {
        preferences.edit().putLong("start", startTimer).apply();
    }

    public long getStop() {
        return preferences.getLong("stop", 0L);
    }

    public void setStop(long stopTimer) {
        preferences.edit().putLong("stop", stopTimer).apply();
    }

    public void setRang(boolean b) {
        preferences.edit().putBoolean("ding_dong", b).apply();
    }

    public boolean hasRang() {
        return preferences.getBoolean("ding_dong", false);
    }

    public int getHomeDayTotal() {
        return preferences.getInt("timeSpent", 0);
    }

    public void setHomeDayTotal(int timeSpent) {
        preferences.edit().putInt("timeSpent", timeSpent).apply();
    }

    public void setAlarmAt(int duration) {
        Log.d("test","notify_after set:"+duration);
        preferences.edit().putInt("notify_after", duration).apply();
    }

    public int getAlarmInterval() {
        int duration = preferences.getInt("notify_after", 460);
        Log.d("test","notify_after get:"+duration);
        return duration;
    }

    public void setAlarmActivated(boolean b) {
        preferences.edit().putBoolean("alarm_activated", b).apply();
    }

    public boolean isAlarmActivated() {
        return preferences.getBoolean("alarm_activated", true);
    }

    public boolean isFirstStartOfTheDay(){
        return preferences.getBoolean("isFirstStartOfTheDay",true);
    }

    public void setFirstStartOfTheDay(boolean firstStart){
        preferences.edit().putBoolean("isFirstStartOfTheDay",firstStart).apply();
    }

    public long getFirstStart(){
        return preferences.getLong("firstStartMilis",0);
    }

    public void setFirstStart(long time){
        preferences.edit().putLong("firstStartMilis",time).apply();
    }

    public int getPreviousDuration(){
        return preferences.getInt("previousDuration",0);
    }

    public void setPreviousDuration(int duration){
        preferences.edit().putInt("previousDuration",duration).apply();
    }
}
