package com.foanapps.alexandru.mtime.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.foanapps.alexandru.mtime.R;
import com.foanapps.alexandru.mtime.fragments.SettingsFragment;

/**
 * Created by alexandru on 7/11/2017.
 */

public class MapPresenter implements View.OnClickListener, DialogInterface.OnClickListener {

    private Context mContext;
private FragmentActivity mActivity;

    public MapPresenter(Context context, FragmentActivity activity) {
        mContext = context;
        mActivity=activity;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

    }


    private void commitFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = mActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.add_edit_layout:
                Log.d("test","add edit click");
                commitFragment(new SettingsFragment());
                break;
            case R.id.delete_layout:
                Log.d("test","delete click");
                break;
            default:
                Log.d("test","default");
                break;
        }
    }
}
