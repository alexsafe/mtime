package com.foanapps.alexandru.mtime.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foanapps.alexandru.mtime.R;
import com.foanapps.alexandru.mtime.presenters.MTrackPresenter;
import com.foanapps.alexandru.mtime.utils.PersistentManager;

import static com.foanapps.alexandru.mtime.utils.Common.HQ;
import static com.foanapps.alexandru.mtime.utils.Common.LOCATION_NAME;
import static com.foanapps.alexandru.mtime.utils.Common.LOGGING_CODE;
import static com.foanapps.alexandru.mtime.utils.Common.LOGGING_KEY;

/**
 * Created by alexandru on 3/11/2017.
 */

public class CurrentFragment extends Fragment {

    private TextView currentDuration;
    private TextView currentLocation;
    private TextView arrivedTime;
    private RelativeLayout masterCurrent;
    int serviceInfoText;
    private LogReceiver mReceiver;
    private PersistentManager pm;
    private MTrackPresenter presenter;


    public static CurrentFragment newInstance() {
        return new CurrentFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceiver = new LogReceiver();
        serviceInfoText = 0;
        pm=new PersistentManager(getContext());
        presenter=new MTrackPresenter(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().registerReceiver(mReceiver, new IntentFilter(LOGGING_CODE));
        return inflater.inflate(R.layout.fragment_current, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currentDuration = (TextView) view.findViewById(R.id.currentDuration);
        currentLocation = (TextView) view.findViewById(R.id.currentLocation);
        arrivedTime = (TextView) view.findViewById(R.id.arrivedTime);
        masterCurrent = (RelativeLayout) view.findViewById(R.id.masterCurrent);
        Log.d("test","onViewCreated text:"+serviceInfoText);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(mReceiver);
    }

    public class LogReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            serviceInfoText = bundle.getInt(LOGGING_KEY);
            String currentLocationName = bundle.getString(LOCATION_NAME);
            String hours=String.valueOf(serviceInfoText/60);
            String minutes=String.valueOf(serviceInfoText%60);
            currentDuration.setText(hours+"h:"+minutes+"m");
            String locationName = "at Work";
            if (!currentLocationName.equals(HQ))
                locationName= "not at work";
            currentLocation.setText("You are "+locationName);

            if (pm.getFirstStart()!=0)
                arrivedTime.setText("You arrived at "+presenter.getDateTime(pm.getFirstStart()));
            masterCurrent.setBackgroundColor(getResources().getColor(R.color.currentInZone));
            if (!currentLocationName.equals(HQ)){
                masterCurrent.setBackgroundColor(getResources().getColor(R.color.currentOffZone));
            }
            //you can call the EventBus from in here
        }
    }
}
