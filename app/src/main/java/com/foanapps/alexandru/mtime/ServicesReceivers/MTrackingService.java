package com.foanapps.alexandru.mtime.ServicesReceivers;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.foanapps.alexandru.mtime.presenters.MTrackPresenter;
import com.foanapps.alexandru.mtime.utils.LocationProvider;
import com.foanapps.alexandru.mtime.utils.PersistentManager;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static com.foanapps.alexandru.mtime.utils.Common.HQ;
import static com.foanapps.alexandru.mtime.utils.Common.LOCATION_NAME;
import static com.foanapps.alexandru.mtime.utils.Common.LOGGING_CODE;
import static com.foanapps.alexandru.mtime.utils.Common.LOGGING_KEY;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by alexandru on 3/11/2017.
 */

public class MTrackingService extends Service {


    private static int RINGINTERVAL = 60; //mins
    private Timer timer;
    private PersistentManager pm;
    private MTrackPresenter presenter;


    public MTrackingService() {

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        MyTimerTask myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, 1000, 5000);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e("test", "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        Log.e("test", "onCreate");
        pm = new PersistentManager(MTrackingService.this);
        presenter = new MTrackPresenter(MTrackingService.this);
    }

    private void sendDurationInfoToFragment(int diffWithCurrent, String locationName) {
        Intent sendIntent = new Intent(LOGGING_CODE);
        sendIntent.putExtra(LOGGING_KEY, diffWithCurrent);
        sendIntent.putExtra(LOCATION_NAME, locationName);
        sendBroadcast(sendIntent);
    }

    private class MyTimerTask extends TimerTask implements LocationProvider.LocationCallback {

        long currentMilliTime;


        @Override
        public void run() {
            currentMilliTime = System.currentTimeMillis() / 1000;
            LocationProvider mLocationProvider = new LocationProvider(getApplicationContext(), this);
            mLocationProvider.connect();
        }


        public void handleNewLocation(Location location) {

            if (location != null) {


                double currentLatitude = Math.toRadians(location.getLatitude());
                double currentLongitude = Math.toRadians(location.getLongitude());
                LatLng latLng = new LatLng(currentLatitude, currentLongitude);

                double home_lat = Math.toRadians(45.539533);
                double home_lon = Math.toRadians(-73.703069);

                // mg
//                double work_lat = Math.toRadians(45.497412);
//                double work_lon = Math.toRadians(-73.656179);

                //bt
                double work_lat = Math.toRadians(45.520066);
                double work_lon = Math.toRadians(-73.852032);

//                double work_lat = Math.toRadians(45.532411); //gl
//                double work_lon = Math.toRadians(-73.622364);


                if (HQ.equals("work")) {
                    if (acos(sin(currentLatitude) * sin(work_lat) + cos(currentLatitude) * cos(work_lat) * cos(work_lon - (currentLongitude))) * 6371 <= 0.1) {
                        setLocationInfo("work");
                    } else {
                        setLocationInfo("void");
                    }
                } else if (acos(sin(currentLatitude) * sin(home_lat) + cos(currentLatitude) * cos(home_lat) * cos(home_lon - (currentLongitude))) * 6371 <= 0.1) {
                    setLocationInfo("home");
                } else {
                    setLocationInfo("void");
                }
            } else {
                setLocationInfo("void");
            }

        }

        public void setLocationInfo(String locationName) {
            String prevLocationName = pm.getLastLocation();

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
            long start = 0L;
            long stop = 0L;
            String mesaj = "";
//            locationName = "work";
//            prevLocationName = "work";
            String status = presenter.setLocation(locationName, prevLocationName);
            start = pm.getStart();
            stop = pm.getStop();
            long diff = Math.abs(stop - start) / 60;
            int diffWithCurrent = 0;
            if (locationName.equals(HQ)) {
                diffWithCurrent = (int) Math.abs(currentMilliTime - start) / 60;
            }
            mesaj = status + " at " + simpleDateFormat.format(calendar.getTime());


            int diffPerDay = pm.getPreviousDuration();


            if (!locationName.equals(prevLocationName)) {
            } else {
                if (locationName.equals(HQ)) {
                    int durationForNotification = pm.getAlarmInterval();
                    diffPerDay = pm.getPreviousDuration() + diffWithCurrent;

//                    diffPerDay = 460;
                    if (diffPerDay != 0 && durationForNotification == diffPerDay && !pm.hasRang() && pm.isAlarmActivated()) {
                        presenter.sendNotification(mesaj, locationName, getBaseContext());
//                        Log.d("test", "sendNotification suna:" + pm.hasRang());
                        pm.setRang(true);
                    }
                    if (diffWithCurrent > durationForNotification) {
                        pm.setRang(false);
                    }
                }
            }
            sendDurationInfoToFragment(diffPerDay, locationName);
            pm.setLastLocation(locationName);
        }


    }

}
