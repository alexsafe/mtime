package com.foanapps.alexandru.mtime.models;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * Created by alexandru on 3/13/2017.
 */

public class Place extends SugarRecord {
    @Unique
    private String placeId;
    private String name;
    private String day;
    private int duration;
    private String arrivedAt;
    private String leftAt;

    // Default constructor is necessary for SugarRecord
    public Place() {

    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String id) {
        this.placeId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getArrivedAt() {
        return arrivedAt;
    }

    public void setArrivedAt(String arrivedAt) {
        this.arrivedAt = arrivedAt;
    }

    public String getLeftAt() {
        return leftAt;
    }

    public void setLeftAt(String leftAt) {
        this.leftAt = leftAt;
    }
}
