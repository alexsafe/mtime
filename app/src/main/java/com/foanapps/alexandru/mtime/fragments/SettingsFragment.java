package com.foanapps.alexandru.mtime.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.foanapps.alexandru.mtime.R;
import com.foanapps.alexandru.mtime.presenters.MTrackPresenter;
import com.foanapps.alexandru.mtime.utils.PersistentManager;
import com.shawnlin.numberpicker.NumberPicker;

/**
 * Created by alexandru on 3/11/2017.
 */

public class SettingsFragment extends Fragment {

    private Switch activated;
    private RelativeLayout timeSettings;
    private NumberPicker hoursPicker;
    private NumberPicker minutesPicker;
    private int hoursPicked;
    private int minutesPicked;
    private PersistentManager pm;
    private MTrackPresenter presenter;

    public SettingsFragment() {

    }

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            pm = new PersistentManager(getActivity().getApplicationContext());
            presenter = new MTrackPresenter(getActivity().getApplicationContext());
        }

        activated = (Switch) view.findViewById(R.id.alarmActivated);
        timeSettings = (RelativeLayout) view.findViewById(R.id.timeSettings);
        hoursPicker = (NumberPicker) view.findViewById(R.id.hourPicker);
        minutesPicker = (NumberPicker) view.findViewById(R.id.minutesPicker);


        hoursPicked = hoursPicker.getValue();
        minutesPicked = minutesPicker.getValue();

        setDurationForAlarm();

        activated.setChecked(true);
        activated.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    timeSettings.setVisibility(View.VISIBLE);
                    pm.setAlarmActivated(true);
                } else {
                    timeSettings.setVisibility(View.GONE);
                    pm.setAlarmActivated(false);
                }
            }
        });

        hoursPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                hoursPicked = newVal;
                setDurationForAlarm();
            }
        });
        minutesPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                minutesPicked = newVal;
                setDurationForAlarm();
            }
        });

        hoursPicker.setValue(presenter.getHoursFromInterval(pm.getAlarmInterval()));
        minutesPicker.setValue(presenter.getMinutesFromInterval(pm.getAlarmInterval()));

    }

    private void setDurationForAlarm() {
        int totalDurationForAlarm = hoursPicked * 60 + minutesPicked;
        if (hoursPicked == 0 && minutesPicked == 0) {
            totalDurationForAlarm = pm.getAlarmInterval();
        }
        pm.setAlarmAt(totalDurationForAlarm);
    }


}
