package com.foanapps.alexandru.mtime.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.foanapps.alexandru.mtime.R;
import com.foanapps.alexandru.mtime.presenters.MapPresenter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class HomeFragment extends Fragment implements GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener {
    private MapView mMapView;
    private FrameLayout navigationHome;
    private GoogleMap googleMap;
    private PopupWindow mRouteStopWindow = null;
    private MapPresenter mapPresenter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        navigationHome = (FrameLayout) rootView.findViewById(R.id.navigation_home);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        mapPresenter = new MapPresenter(getContext(), getActivity());

        if (googleMap != null) {
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    Log.d("tag", "long another click at " + latLng);
                    googleMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(" New lcoation ")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                }
            });
        }
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(-34, 151);
//                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
//                        mRouteStopWindow.show;
                    }
                });

                googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {
                        Log.d("tag", "long another click at " + latLng);
                        googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(" New location ")

                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                               final View routeView = inflater.inflate(R.layout.poi_actions_layout, null);
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.MATCH_PARENT,
                                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.addRule(RelativeLayout.ALIGN_BASELINE);
                                routeView.setLayoutParams(layoutParams);
                                Log.d("test", "glicked button");

                                if (mRouteStopWindow == null) {
                                    mRouteStopWindow = new PopupWindow(
                                            routeView,
                                            RelativeLayout.LayoutParams.MATCH_PARENT,
                                            150
                                    );
                                    // Closes the popup window when touch outside.
                                    mRouteStopWindow.setOutsideTouchable(true);
                                    mRouteStopWindow.setFocusable(true);
                                    // Removes default background.
                                    mRouteStopWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                }

                                if (Build.VERSION.SDK_INT >= 21) {
                                    mRouteStopWindow.setElevation(20.0f);
                                }

                                RelativeLayout addEdit = (RelativeLayout) routeView.findViewById(R.id.add_edit_layout);
                                RelativeLayout delete = (RelativeLayout) routeView.findViewById(R.id.delete_layout);
                                addEdit.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        navigationHome.removeView(routeView);
                                        commitFragment(new SettingsFragment());
                                    }
                                });
                                delete.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        navigationHome.removeView(routeView);
                                    }
                                });
                                navigationHome.addView(routeView);
//                                routeView.showAtLocation(navigationHome, Gravity.BOTTOM | Gravity.LEFT, 0, 0);
                                return false;
                            }
                        });
                    }
                });
            }
        });

        return rootView;
    }

    private void commitFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Log.d("tag", "long click at " + latLng);
        googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(" New lcoation ")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d("Test", "muie marker");
        return false;
    }
}
